package cd.home.employee.controller;

import cd.home.employee.entity.Employee;
import cd.home.employee.exception.ResourceNotFoundException;
import cd.home.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Map;

import javax.websocket.server.PathParam;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/")
    public String index() {
        return "Welcome";
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeService.findAll();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id) {
        Employee employee = employeeService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("employee with this id not found!!!"));
        return ResponseEntity.ok(employee);
    }

    @PostMapping("/employees")
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity editEmployee(@RequestBody Employee employee, @PathVariable("id") Long id) {
        Employee foundEmployee = employeeService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("employee with this id not found!!!"));
        foundEmployee.setFirstName(employee.getFirstName());
        foundEmployee.setLastName(employee.getLastName());
        Employee updatedEmployee = employeeService.save(foundEmployee);
        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/employees/{id}")
    public ResponseEntity <Map<String,Boolean>> deleteEmployee(@PathVariable("id") Long id) {
        Employee foundEmployee = employeeService.findById(id).orElseThrow(() -> new ResourceNotFoundException("employee with this id not found!!!"));
        employeeService.delete(foundEmployee);
        Map<String,Boolean> response= new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);

    }
}
